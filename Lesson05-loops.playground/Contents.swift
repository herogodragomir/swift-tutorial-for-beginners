import UIKit

var str = "Hello, playground"

var sum = 0
for counter in 1...5 {
    
    sum += counter
}
print(sum)

// For in Loop
// for counter in lower...upper {
//     some code
// }
