import UIKit

var str = "Hello, playground"

func addTwoNumbers() {
    let a = 1
    let b = 2
    let c = a + b
    print(c)
}
addTwoNumbers()

func subtractTwoNumbers() {
    let d = 5
    let e = 1
    let f = d - e
    print(f)
}
subtractTwoNumbers()

// Function
// A block of code with a given name that can be executed on demand by calling that name

// Basic Function Syntax
// func name() {
//     some code
// }

// Recap:
// * Functions help us organize our code into executable blocks.
// * The func keyword defines a function.
// * We use the function name to call it.
// * The scope of a function is in between its curly brackets.
// * Variables and constants created inside a function are only accessible inside that function.
