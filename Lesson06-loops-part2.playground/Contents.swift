import UIKit

var str = "Hello, playground"

var counter = -5

while counter > 0 {
    print("hello")
    counter -= 1
}

var counter2 = -5

repeat {
    print("hello from repeat while loop")
    counter2 -= 1
} while counter2 > 0

// Repeat-While Loop
// repeat {
//     some code
// } while condition

// Recap:
// While Loop
// Repeat-While Loop

// Recap:
// While Loop checks the condition FIRST

// Recap:
// While Loop runs the code FIRST and then checks the condition for looping
