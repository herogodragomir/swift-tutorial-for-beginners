import UIKit

var str = "Hello, playground"

let a = 25
let b = 10
let c = 1

if (a <= 10 || b > 5) && c != 1 {
    print("branch 1")
}
else if a < 15 {
    print("branch 2")
}
else if a > 30 {
    print("branch 3")
} else {
    print("catch all")
}

// IF Statements
// if condition {
//     some code
// }

// ELSE IF Clause
// if condition1 {
//     some code
// }
// else if condition2 {
//     some code
// }
// else {
//     some code
// }

// Recap: Use IF Statements to conditionally execute code
// Recap: Add alternative conditions with the ELSE IF clause
// Recap: Use the ELSE clause as a final alternative that'll execute if none of the conditions are true
// Conditions are checked from top to bottom
