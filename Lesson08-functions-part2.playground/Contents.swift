import UIKit

var str = "Hello, playground"

func addTwoNumbers(_ number1:Int, _ number2:Int) -> Int {
    let a = number1
    let b = number2
    let c = a + b
    
    return c
}

let sum = addTwoNumbers(5, 7)
print(sum)

// Function Syntax: Return Values
// func name() -> DataType {
//     some code
//     return someValue
// }

// Function Syntax: 1 Parameter
// func name(argumentLabel parameterName: DataType) {
//     some code
// }

// Function Syntax: Multiple Parameters
// func name(arg1 param1: DataType1, arg2 param2: DataType2) {
//     some code
// }

// Recap:
// * Return value of a function allows you to return data using the Return keyword
// * Parameters let you supply data into your function when you call it
