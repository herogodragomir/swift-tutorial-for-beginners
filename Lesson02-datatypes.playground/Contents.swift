import UIKit

// Data types
// String: Piece of text
// Int: Whole numbers positive and negative
// Float: Decimal numbers
// Double: Large decimal numbers
// Bool: True or false

var str:String = "Tom" // Collection of characters
var anInteger:Int = 100 // Represent number positive and negative
var aFloat:Float = 0.2 // Is number with fraction
var aDouble:Double = 0.3 // Is different with double
var aBool:Bool = true // or false
