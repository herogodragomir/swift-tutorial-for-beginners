import UIKit

var str = "Hello, playground"

var firstname = "Tom"
print(firstname)

var stockprice = 100
print(stockprice)

stockprice = 50
print(stockprice)

let lastname = "Smith"

// 1. Variables and constants are used to keep track of data in your app.
// 2. Use the "var" and "let" keywords to declare new variables and constants.
// 3. Use the equal sign to assign data to a variable or constant.
// 4. Use Camel Case as a best practice for naming your variables and constants.
// 5. Constants are like variables but you can't reassign data to them after the initial assignment.
