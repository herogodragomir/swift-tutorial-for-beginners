import UIKit

class Employee {
    var name = ""
    var salary = 0
    var role = ""
    
    func doWork() {
        print("Hi my name is \(name) and I'm doing work")
        salary += 1
    }
}

let a:Int = 10
let b:String = "Ted"
let c:Employee = Employee()

c.name = "Tom"
c.role = "Art Director"
c.salary = 1000
print(c.salary)

c.doWork()

var d = Employee()
d.name = "Sarah"
d.role = "Manager"
d.salary = 1000

d.doWork()

// Class Syntax
// class name {
//
// }

// Classes and Objects
// A class definition is like a template or blueprint
// When you create an actual tangible instance of the class, that instance is called an "object"
