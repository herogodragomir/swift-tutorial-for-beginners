import UIKit

var str = "Hello, playground"

let chr = "c"

switch chr {
case "a":
    print("this is an a")
case "b", "c":
    print("this is an b or c")
default:
    print("This is the fallback")
}

// SWITCH Statements
//
// switch value to consider {
// case value1:
//     some code
// case value2:
//     some code
// default:
//     some code
