import UIKit


class Person {
    
    var name = ""
    
    init(_ name: String) {
        self.name = name
    }
}

class Employee: Person {
    
    var salary = 0
    var role = ""
    
    func doWork() {
        print("Hi my name is \(name) and I'm doing work")
        salary += 1
    }
}

class Manager: Employee {
    
    var teamSize = 0
    var bonus: Int {
        // This is a computer property
        // When it's accesses, the code is here will run
        // Then we'll return the value
        
        return teamSize * 1000
    }
    
    init(_ name:String, _ team:Int) {
        
        // This calls the init of the Employee class
        super.init(name)
        
        // Additional init work
        teamSize = team
    }
    
    override func doWork() {
        super.doWork()
        print("I'm managing people")
        salary += 2
    }
    
    func firePeople() {
        print("I'm firing people")
    }
}

let m = Manager("Kate", 11)
print(m.bonus)
